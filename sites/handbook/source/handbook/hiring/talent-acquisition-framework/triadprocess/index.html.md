---
layout: handbook-page-toc
title: "Triad Process"
description: "An overview on what the Triad Process is and who is involved."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### The Triad Process

The Triad process exists to connect the various functional representatives from the PBP group, Finance and Talent Acquisition together to ensure seamless collaboration, reporting and metrics tracking. 

#### Roles and Responsibilities
This group meets once a week to audit the centralized headcount report for their area. This centralized report captures a weekly download from both Adaptive and Greenhouse. Seperate sheets per department/division should be avoided in order to drive towards a single source of truth so that we can ensure transparency and correct budget tracking.

**DRI**: The relevant TA leader is the DRI in the meeting based on the fact that they are ultimately responsible for TA metrics and reporting for their group. TA leaders will drive the conversation to ensure three key outcomes (1) our planning and recruiting tools are aligned (2) our hiring forecasts are current and (3) all requisition changes are communicated and updated in Adaptive through sharing the **Triad Interlock Weekly Audit Report**. 

**Contributors**: The remaining triad members (inclusive of both People Business Partners and Finance Buisness Partners) will be key contributors for the interlock meeting. Finance Buisness Partners are responsible for providing context where needed and ensuring Adaptive reflects all influenced inputs (ie: target start dates, requisition changes, etc.). People Business Partners are responsible for collaborating with FP+A and TA to help enable the partnership with business leaders.  

#### Outcomes
The successful output of the Triad meeting is aligned reporting, and data hygiene between systems and G&A parties. We will continue to have one source of truth for requisition status, hiring plan and hire data information enabled by this collaborative effort between TA, FP&A and PBPs.  

#### Timeline
**Mondays - 12pm PT** FP&A and TA delegates refresh inputs to weekly report

**Wednesdays - 5pm PT** TA Directors (DRIs) run audit and surface inconsistencies. Action where available and drive conversation at interlock meeting when needed.

**Thursday - 5pm PT** FP&A Partners update Adaptive as needed for aligned division.

**Friday** Weekly Snapshot set to Senior Leadership.
